# Prerequisites
`mkdir -p items/vanilla` in repo root

download to `items/vanilla/`:
https://artifactory.median-xl.com/repository/assets/items/vanilla/dc6.zip

download to `items/`:
https://artifactory.median-xl.com/repository/assets/items/dc6.zip

You need access to diablo .txt files, extracted somewhere

you need to install

*  node + npm
*  wine (if running on linux, for using dc6con)
*  get dc6con from phrozen keep
*  install FreeTexturePacker and the free-tex-packer-cli
* python3 and Pillow (image processing python module)

```
python3 -m pip install Pillow
```

# Setup
uncomment the env vars in `template.env` and fill it in according to your environment and then save that as `.env`.

before first build, run `npm install` from repo root

# Building
run `make`

output goes into `~/atlas` dir, you'll find a png and css for each transform
found in the `armor.txt` and `weapons.txt` in the mpq's
