const fs = require('fs')
const path = require('path')

let ARGS            = process.argv.slice(2),
    SRCDIR          = ARGS[0],
    DSTDIR          = ARGS[1],
    JSONPATH        = ARGS[2],
    JSONCONTENTS    = fs.readFileSync(JSONPATH),
    JSONDATA        = JSON.parse(JSONCONTENTS),
    d               = JSONDATA // for brevity

let mv_if_exists = (srcdir, dstdir, filename) => {
    let srcpath = path.join(srcdir, filename),
        dstpath = path.join(dstdir, filename),
        exists = fs.existsSync(srcpath)

    if (exists) {
        fs.renameSync(srcpath, dstpath)
    }

}

let records = {}

d.map(record=>{
    let invfile     = record.invfile,
        filename    = invfile + '.png',
        transform   = record.InvTrans

    if ((transform == undefined) || (transform =='')) {
        transform = 0
    }

    if (records[invfile] == undefined)
        records[invfile] = {filename: filename, dst:'', transforms: []}

    records[invfile].transforms.push(transform)

})

Object.values(records).map(r=>{
    let transformStrs = [...r.transforms],
        transformInts = transformStrs.map(t=>parseInt(t)),
        highestTransform = ''+Math.max(...transformInts)

    let dstdir      = path.join(DSTDIR, highestTransform)

    mv_if_exists(SRCDIR, dstdir, r.filename)
})
