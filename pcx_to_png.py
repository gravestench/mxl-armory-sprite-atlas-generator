import sys
import os
from math import sqrt
from PIL import Image

pcxFiles    = sys.argv[1:]

for filename in pcxFiles:
    dstPath = filename.split('.')[0]+'.png'
    Image.open(filename).save(dstPath, transparency=0, optimize=0)

