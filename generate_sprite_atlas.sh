#!/bin/bash

TEMPLATE=ftpp/transform-template.ftpp

for transform in $(ls transforms)
do
    TRANSFORM_FTPP=${TEMPLATE%-*}-${transform}.ftpp
    cp $TEMPLATE $TRANSFORM_FTPP
    sed -i "s/TRANSFORM/$transform/" "$TRANSFORM_FTPP"
    sed -i "s/NAME/transform-$transform/" "$TRANSFORM_FTPP"
    free-tex-packer-cli --project ${TRANSFORM_FTPP} > /dev/null 2>&1
    rm ${TRANSFORM_FTPP}
done
