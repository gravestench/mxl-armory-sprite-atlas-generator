import sys
from math import sqrt
from PIL import Image
from operator import itemgetter

def getBytesFromFile(filename):
    return open(filename, "rb").read()

# weighted diff
# without this, output images ended up being slightly red
def wd(a,b,w):
    return (b-a)*w

def getClosestColorIndex(r,g,b,p):
    distances = [sqrt(wd(r,c[0],1)**2 + wd(g,c[1],1)**2 + wd(b,c[2],1)**2) for c in p]
    closest = min(enumerate(distances), key=itemgetter(1))[0]
    return closest

atlasImagePath  = sys.argv[1]
palettePath     = sys.argv[2]
transformPath   = sys.argv[3]

transform       = bytearray(getBytesFromFile(transformPath))
srcPngRaw       = bytearray(getBytesFromFile(atlasImagePath))
srcImage        = Image.open(atlasImagePath).convert('P')

dstPaletteRaw   = [b for b in bytearray(getBytesFromFile(palettePath))]
lenDst          = len(dstPaletteRaw)
srcPaletteRaw   = srcImage.getpalette()
lenSrc          = len(srcPaletteRaw)

srcPalette      = [srcPaletteRaw[i*3:(i+1)*3] for i in range((lenSrc+2)//3)]
dstPalette      = [dstPaletteRaw[i*3:(i+1)*3] for i in range((lenDst+2)//3)]

mapping = [getClosestColorIndex(c[0],c[1],c[2],dstPalette) for c in srcPalette]

width           = srcImage.width
height          = srcImage.height
srcImageData    = srcImage.getdata()
dstImageData    = [mapping[idx] for idx in srcImageData]

srcImage.putdata(dstImageData)
srcImage.putpalette([b for b in dstPaletteRaw])
srcImage.save(atlasImagePath, transparency=0, optimize=0)
