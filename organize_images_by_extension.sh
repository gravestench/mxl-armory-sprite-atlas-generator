#!/bin/bash

DC6DIR="dc6"
PCXDIR="pcx"
PNGDIR="png"

[ -d ${DC6DIR} ] && rm -rf ${DC6DIR}
[ -d ${PCXDIR} ] && rm -rf ${PCXDIR}
[ -d ${PNGDIR} ] && rm -rf ${PNGDIR}

mkdir -p $DC6DIR $PCXDIR $PNGDIR

for name in *.{dc6,DC6}
do
    mv $name ${DC6DIR}/ > /dev/null 2>&1
done 

for name in *.{pcx,PCX}
do
    mv $name ${PCXDIR}/ > /dev/null 2>&1
done 

for name in *.{png,PNG}
do
    mv $name ${PNGDIR}/ > /dev/null 2>&1
done 

exit 0
