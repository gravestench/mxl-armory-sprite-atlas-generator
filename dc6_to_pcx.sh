#!/bin/bash

DC6CON=$1

echo ""
for name in *.{dc6,DC6}
do
    printf " ${name%.*}"
    wine $DC6CON $name  > /dev/null 2>&1
done

printf "\r\rDONE\r"
echo ""

exit 0
