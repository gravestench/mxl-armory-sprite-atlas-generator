# dont change this file, edit template.env and save a copy as `.env`

include .env
export

# the rest of the shit that's in this makefile shouldn't need to be changed.
# just make sure you have the dependencies met.
MPQ_EXCEL_DIR=data/global/excel

# the names of the txt files without the file extension
MPQ_ARMOR=armor
MPQ_WEAPON=weapons

# the palette and palette transform data files are just rgb tuples ib binary
PALETTE_DIR=palette
MPQ_BASE_PALETTE_DIR=${MPQ_DIR}/${MPQ_ORIG}/data/global/palette/ACT1
MPQ_ITEM_PALETTE_DIR=${MPQ_DIR}/${MPQ_ORIG}/data/global/items/Palette

PALETTE_0=pal.dat
PALETTE_1=grey.dat
PALETTE_2=grey2.dat
PALETTE_5=greybrown.dat
PALETTE_8=invgreybrown.dat

# tsv-parser submodule -- outputs xml and json, easier for me to script
TSV_DIR=tsv-parser
TSV_INPUT=${TSV_DIR}/input
TSV_OUTPUT=${TSV_DIR}/output

MXL_ARMOR_TXT=${MPQ_DIR}/${MPQ_MXL}/${MPQ_EXCEL_DIR}/${MPQ_ARMOR}.txt
MXL_WEAPON_TXT=${MPQ_DIR}/${MPQ_MXL}/${MPQ_EXCEL_DIR}/${MPQ_WEAPON}.txt

PATCH_ARMOR_TXT=${MPQ_DIR}/${MPQ_PATCH}/${MPQ_EXCEL_DIR}/${MPQ_ARMOR}.txt
PATCH_WEAPON_TXT=${MPQ_DIR}/${MPQ_PATCH}/${MPQ_EXCEL_DIR}/${MPQ_WEAPON}.txt

EXP_ARMOR_TXT=${MPQ_DIR}/${MPQ_EXP}/${MPQ_EXCEL_DIR}/${MPQ_ARMOR}.txt
EXP_WEAPON_TXT=${MPQ_DIR}/${MPQ_EXP}/${MPQ_EXCEL_DIR}/${MPQ_WEAPON}.txt

ORIG_ARMOR_TXT=${MPQ_DIR}/${MPQ_ORIG}/${MPQ_EXCEL_DIR}/${MPQ_ARMOR}.txt
ORIG_WEAPON_TXT=${MPQ_DIR}/${MPQ_ORIG}/${MPQ_EXCEL_DIR}/${MPQ_WEAPON}.txt

# https://d2mods.info/forum/kb/viewarticle?a=57
TRANSFORM_NONE=0
TRANSFORM_GREY=1
TRANSFORM_GREY2=2
TRANSFORM_GREYBROWN=5
TRANSFORM_INVGREYBROWN=8
TRANSFORM_DIR=transforms

# shorthand for those dirs
TNONE=${TRANSFORM_DIR}/${TRANSFORM_NONE}
TGREY=${TRANSFORM_DIR}/${TRANSFORM_GREY}
TGREY2=${TRANSFORM_DIR}/${TRANSFORM_GREY2}
TGREYBROWN=${TRANSFORM_DIR}/${TRANSFORM_GREYBROWN}
TINVGREYBROWN=${TRANSFORM_DIR}/${TRANSFORM_INVGREYBROWN}

ATLAS_DIR=atlas

.PHONY: verify_tools verify_mpq

all: \
	init_submodules \
	setup_tsv_parser \
	verify_tools \
	verify_mpq \
	extract_artifactory_dc6_zip \
	convert_dc6_to_pcx \
	convert_pcx_to_png \
	organize_images_by_extension \
	parse_transforms_to_json \
	organize_by_transform \
	prepare_palette_files \
	make_sprite_atlas \
	remap_atlas_palette_to_base_palette

init_submodules:
	@git submodule update --init --recursive  > /dev/null 2>&1

setup_tsv_parser:
	@cd tsv-parser; npm install  > /dev/null 2>&1

verify_tools:
	@echo "\nChecking for wine..."
	@command -v wine >/dev/null 2>&1 || \
		{ echo >&2 "wine not found"; exit 1; }
	
	@echo "Checking for node..."
	@command -v node >/dev/null 2>&1 || \
		{ echo >&2 "nodejs not found"; exit 1; }
	
	@echo "Checking for npm..."
	@command -v npm >/dev/null 2>&1 || \
		{ echo >&2 "npm not found"; exit 1; }
	
	@echo "Checking for FreeTexturePacker..."
	@command -v FreeTexturePacker >/dev/null 2>&1 || \
		{ echo >&2 "FreeTexturePacker not found"; exit 1; }
	
	@echo "Checking for free-tex-packer-cli..."
	@command -v free-tex-packer-cli >/dev/null 2>&1 || \
		{ echo >&2 "free-tex-packer-cli not found"; exit 1; }
	
	@echo "Checking for dc6con.exe..."
	@[ -f ${DC6_EXE_PATH} ] || \
		{ echo >&2 "dc6con.exe not found"; exit 1; }
	@echo "Tool check [PASSED]\n"

verify_mpq:
	@echo "Checking for extracted MPQ's..."
	@[ -d ${MPQ_DIR}/${MPQ_MXL} ] || \
		{ echo 'No MXL dir found'; exit 1; }
	
	@[ -d ${MPQ_DIR}/${MPQ_PATCH} ] || \
		{ echo 'No d2patch dir found'; exit 1; }
	
	@[ -d ${MPQ_DIR}/${MPQ_EXP} ] || \
		{ echo 'No d2exp dir found'; exit 1; }
	
	@[ -d ${MPQ_DIR}/${MPQ_ORIG} ] || \
		{ echo 'No d2data dir found'; exit 1; }
	
	@[ -f ${MXL_ARMOR_TXT} ] || \
		{ echo >&2 "${MXL_ARMOR_TXT} not found"; exit 1; }
	@[ -f ${MXL_WEAPON_TXT} ] || \
		{ echo >&2 "${MXL_WEAPON_TXT} not found"; exit 1; }
	
	@[ -f ${PATCH_ARMOR_TXT} ] || \
		{ echo >&2 "${PATCH_ARMOR_TXT} not found"; exit 1; }
	@[ -f ${PATCH_WEAPON_TXT} ] || \
		{ echo >&2 "${PATCH_WEAPON_TXT} not found"; exit 1; }
	
	@[ -f ${EXP_ARMOR_TXT} ] || \
		{ echo >&2 "${EXP_ARMOR_TXT} not found"; exit 1; }
	@[ -f ${EXP_WEAPON_TXT} ] || \
		{ echo >&2 "${EXP_WEAPON_TXT} not found"; exit 1; }
	
	@[ -f ${ORIG_ARMOR_TXT} ] || \
		{ echo >&2 "${ORIG_ARMOR_TXT} not found"; exit 1; }
	@[ -f ${ORIG_WEAPON_TXT} ] || \
		{ echo >&2 "${ORIG_WEAPON_TXT} not found"; exit 1; }
	
	@echo "MPQ check [PASSED]\n"

extract_artifactory_dc6_zip:
	@echo "Extracting DC6 files"
	@./extract_dc6.sh ${DC6_ORIG_ZIP} ${DC6_MXL_ZIP}  > /dev/null 2>&1

convert_dc6_to_pcx:
	@echo "Converting DC6 files to PCX"
	@./dc6_to_pcx.sh ${DC6_EXE_PATH}

convert_pcx_to_png:
	@echo "Converting PCX files to PNG"
	@./pcx_to_png.sh > /dev/null 2>&1

organize_images_by_extension:
	@echo "Organizing images by file extension"
	@./organize_images_by_extension.sh > /dev/null 2>&1

parse_transforms_to_json:
	@echo "Parsing transforms to json"
	@mkdir -p ${TSV_INPUT}
	@mkdir -p ${TSV_OUTPUT}
	
	@# copy the files over into the tsv-parser input dir
	@cat ${MXL_ARMOR_TXT}	> ${TSV_INPUT}/${MPQ_MXL}_${MPQ_ARMOR}.txt
	@cat ${MXL_WEAPON_TXT}   > ${TSV_INPUT}/${MPQ_MXL}_${MPQ_WEAPON}.txt
	
	@cat ${PATCH_ARMOR_TXT}  > ${TSV_INPUT}/${MPQ_PATCH}_${MPQ_ARMOR}.txt
	@cat ${PATCH_WEAPON_TXT} > ${TSV_INPUT}/${MPQ_PATCH}_${MPQ_WEAPON}.txt
	
	@cat ${EXP_ARMOR_TXT}	> ${TSV_INPUT}/${MPQ_EXP}_${MPQ_ARMOR}.txt
	@cat ${EXP_WEAPON_TXT}   > ${TSV_INPUT}/${MPQ_EXP}_${MPQ_WEAPON}.txt
	
	@cat ${ORIG_ARMOR_TXT}   > ${TSV_INPUT}/${MPQ_ORIG}_${MPQ_ARMOR}.txt
	@cat ${ORIG_WEAPON_TXT}  > ${TSV_INPUT}/${MPQ_ORIG}_${MPQ_WEAPON}.txt
	
	@cd ${TSV_DIR}; node index.js

organize_by_transform:
	@echo "Organizing images by transform"
	@if [ -d ${TRANSFORM_DIR} ]; then rm -rf ${TRANSFORM_DIR}; fi
	
	@mkdir -p ${TNONE}
	@mkdir -p ${TGREY}
	@mkdir -p ${TGREY2}
	@mkdir -p ${TGREYBROWN}
	@mkdir -p ${TINVGREYBROWN}
	
	@# 1x1 px files break the texture packer...? no idea why.
	@rm png/inv?x?.png 
	@rm png/dye-rainbow.png 
	
	@# default is to not have a transform, move them all there first
	@cp png/*.png ${TRANSFORM_DIR}/${TRANSFORM_NONE}/
	
	@# iterate through parsed json files
	@# moving files from TRANSFORM_NONE dir into wherever else
	
	@# first, check data from mxl.mpq
	@node organize_by_transforms.js ${TNONE} ${TRANSFORM_DIR} \
		${TSV_OUTPUT}/json/${MPQ_MXL}_${MPQ_ARMOR}.json
	@node organize_by_transforms.js ${TNONE} ${TRANSFORM_DIR} \
		${TSV_OUTPUT}/json/${MPQ_MXL}_${MPQ_WEAPON}.json
	
	@# then patch_d2
	@node organize_by_transforms.js ${TNONE} ${TRANSFORM_DIR} \
		${TSV_OUTPUT}/json/${MPQ_PATCH}_${MPQ_ARMOR}.json
	@node organize_by_transforms.js ${TNONE} ${TRANSFORM_DIR} \
		${TSV_OUTPUT}/json/${MPQ_PATCH}_${MPQ_WEAPON}.json
	
	@# then d2exp
	@node organize_by_transforms.js ${TNONE} ${TRANSFORM_DIR} \
		${TSV_OUTPUT}/json/${MPQ_EXP}_${MPQ_ARMOR}.json
	@node organize_by_transforms.js ${TNONE} ${TRANSFORM_DIR} \
		${TSV_OUTPUT}/json/${MPQ_EXP}_${MPQ_WEAPON}.json
	
	@# then d2data
	@node organize_by_transforms.js ${TNONE} ${TRANSFORM_DIR} \
		${TSV_OUTPUT}/json/${MPQ_ORIG}_${MPQ_ARMOR}.json
	@node organize_by_transforms.js ${TNONE} ${TRANSFORM_DIR} \
		${TSV_OUTPUT}/json/${MPQ_ORIG}_${MPQ_WEAPON}.json
	
	
	@# remove empty transform dirs
	@rmdir --ignore-fail-on-non-empty ${TRANSFORM_DIR}/*
	

prepare_palette_files:
	@mkdir -p ${PALETTE_DIR}
	@# just copying to local dir, renaming extension to .bin
	@cp ${MPQ_BASE_PALETTE_DIR}/${PALETTE_0} ${PALETTE_DIR}/0.bin
	@cp ${MPQ_ITEM_PALETTE_DIR}/${PALETTE_1} ${PALETTE_DIR}/1.bin
	@cp ${MPQ_ITEM_PALETTE_DIR}/${PALETTE_2} ${PALETTE_DIR}/2.bin
	@cp ${MPQ_ITEM_PALETTE_DIR}/${PALETTE_5} ${PALETTE_DIR}/5.bin
	@cp ${MPQ_ITEM_PALETTE_DIR}/${PALETTE_8} ${PALETTE_DIR}/8.bin

make_sprite_atlas:
	@echo "Generating sprite atlas files"
	@if [ -d ${ATLAS_DIR} ]; then rm -rf ${ATLAS_DIR}; fi
	@mkdir -p ${ATLAS_DIR}
	@./generate_sprite_atlas.sh

remap_atlas_palette_to_base_palette:
	@./remap_all_atlas_palettes.sh ${PALETTE_DIR}/0.bin

