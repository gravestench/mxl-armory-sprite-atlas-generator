#!/bin/bash

PALETTE=$1

for atlas in $(ls atlas/transform-?.png)
do 
    BASENAME=${atlas%.*}
    TRANSFORM=palette/${BASENAME: -1}.bin
    python3 force_remap_png_palette.py ${atlas} $PALETTE $TRANSFORM
    cat $TRANSFORM >> $atlas
done
