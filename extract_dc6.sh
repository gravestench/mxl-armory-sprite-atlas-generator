#!/bin/bash

VANILLA_DC6_ZIP=$1
MXL_DC6_ZIP=$2

unzip -o $VANILLA_DC6_ZIP -d .  > /dev/null 2>&1
unzip -o $MXL_DC6_ZIP -d .  > /dev/null 2>&1

for x in *"#"*; do
  mv -- "$x" "${x//#/shrine_}" 2>/dev/null
done

for x in *"@"*; do
  mv -- "$x" "${x//@/vessel_}" 2>/dev/null
done
