#!/bin/bash

for file in $(ls atlas/*.png)
do
    convert $file -remap palette/indexed.png $file
done
